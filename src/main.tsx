import { StrictMode } from "react";
import * as ReactDOM from "react-dom/client";
import { Button } from "./components/ui/button";

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);

root.render(
  <StrictMode>
    <Button>Tauri is for nerds</Button>
  </StrictMode>,
);
